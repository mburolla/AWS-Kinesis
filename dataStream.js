//
// Name: dataStream.js
// Auth: Martin Burolla
// Date: 6/5/2018
// Desc: Uploads a record to a data stream.
//

const AWS = require( 'aws-sdk' );
AWS.config.update({ region: 'us-east-1' });

const kinesis = new AWS.Kinesis();

const data = {
  Data: 'Going to the gym with Gus.'
}

const params = {
  Data: new Buffer(JSON.stringify(data)),
  PartitionKey: '1', 
  StreamName: 'MartyDataStream'
};

kinesis.putRecord(params, (err, data) => {
  console.log(err, data);
});
