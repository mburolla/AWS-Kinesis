//
// Name: firehoseDeliveryStream.js
// Auth: Martin Burolla
// Date: 6/5/2018
// Desc: Uploads a record to a Firehose Delivery stream.
//

const AWS = require( 'aws-sdk' );
AWS.config.update({ region: 'us-east-1' });

const fireHose = new AWS.Firehose();

const data = {
  message: 'This is my data. Hello world.'
}

const params = {
  DeliveryStreamName: 'MartyStream1', 
  Record: { Data: new Buffer(JSON.stringify(data)) }
}

fireHose.putRecord(params, (err, data) => {
  console.log(err, data);
});
